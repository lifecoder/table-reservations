require 'rails_helper'

RSpec.describe TableReservation, type: :model do
  context 'Correct input' do
    it 'Creates reservation' do
      reservation = FactoryGirl.create(:table_reservation)

      expect(TableReservation.count).to eq(1)
    end

    it 'Creates non-intersecting reservation' do
      reservation1 = FactoryGirl.create(:table_reservation,
                                        start_time: '2015-05-08 23:12:34',
                                        end_time:   '2015-05-08 23:22:34'
      )
      reservation2 = FactoryGirl.create(:table_reservation,
                                       start_time: '2015-05-08 23:32:34',
                                       end_time:   '2015-05-08 23:52:34'
      )

      expect(reservation2).to be_valid
      expect(TableReservation.count).to eq(2)
    end

    it 'Allow self overlap on update' do
      reservation = FactoryGirl.create(:table_reservation)

      expect{reservation.save!}.not_to raise_error
    end
  end

  context 'Missed field' do
    it 'Does not save reservation' do
      fields = [ :start_time, :end_time, :table_number ]

      fields.each do |field|
        reservation = FactoryGirl.build(:table_reservation, field => '')

        expect(reservation).not_to be_valid
      end

    end
  end

  context 'Overlapping time input' do
    it 'Should fail for outer overlap' do
      test_overlap '2015-05-08 23:32:34', '2015-05-08 23:52:34'
    end
    it 'Should fail for inner overlap' do
      test_overlap '2015-05-08 23:35:34', '2015-05-08 23:40:34'
    end
    it 'Should fail for partial overlap on the start date' do
      test_overlap '2015-05-08 23:32:34', '2015-05-08 23:35:34'
    end
    it 'Should fail for partial overlap on the end date' do
      test_overlap '2015-05-08 23:35:34', '2015-05-08 23:52:34'
    end
  end

  context 'Saving model' do
    it 'Should check the End date is greater than the Start date' do
      reservation = FactoryGirl.build(:table_reservation,
                                       start_time: '2015-05-08 23:32:34',
                                       end_time:   '2015-05-08 23:22:34'
      )

      expect(reservation).not_to be_valid
    end
  end

private

  def test_overlap start_time, end_time
    reservation1 = FactoryGirl.create(:table_reservation)
    reservation2 = FactoryGirl.build(:table_reservation,
                                     start_time: '2015-05-08 23:32:34',
                                     end_time:   '2015-05-08 23:52:34'
    )

    expect(reservation2).not_to be_valid
    expect(reservation2.errors[:base].length).to eq(1)
  end
end
