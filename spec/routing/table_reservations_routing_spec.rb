require "rails_helper"

RSpec.describe TableReservationsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/table_reservations").to route_to("table_reservations#index")
    end

    it "routes to #new" do
      expect(:get => "/table_reservations/new").to route_to("table_reservations#new")
    end

    it "routes to #show" do
      expect(:get => "/table_reservations/1").to route_to("table_reservations#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/table_reservations/1/edit").to route_to("table_reservations#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/table_reservations").to route_to("table_reservations#create")
    end

    it "routes to #update" do
      expect(:put => "/table_reservations/1").to route_to("table_reservations#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/table_reservations/1").to route_to("table_reservations#destroy", :id => "1")
    end

  end
end
