require 'rails_helper'

RSpec.describe "table_reservations/edit", type: :view do
  before(:each) do
    @table_reservation = assign(:table_reservation, FactoryGirl.create(:table_reservation))
  end

  it "renders the edit table_reservation form" do
    render

    assert_select "form[action=?][method=?]", table_reservation_path(@table_reservation), "post" do

      assert_select "input#table_reservation_table_number[name=?]", "table_reservation[table_number]"
    end
  end
end
