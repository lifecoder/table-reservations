require 'rails_helper'

RSpec.describe "table_reservations/index", type: :view do
  before(:each) do
    assign(:table_reservations, [
      FactoryGirl.create(:table_reservation),
      FactoryGirl.create(:table_reservation, table_number: 2)
    ])
  end

  it "renders a list of table_reservations" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 1
    assert_select "tr>td", :text => 2.to_s, :count => 1
  end
end
