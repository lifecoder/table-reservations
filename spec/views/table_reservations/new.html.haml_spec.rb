require 'rails_helper'

RSpec.describe "table_reservations/new", type: :view do
  before(:each) do
    assign(:table_reservation, TableReservation.new(
      :table_number => 1
    ))
  end

  it "renders new table_reservation form" do
    render

    assert_select "form[action=?][method=?]", table_reservations_path, "post" do

      assert_select "input#table_reservation_table_number[name=?]", "table_reservation[table_number]"
    end
  end
end
