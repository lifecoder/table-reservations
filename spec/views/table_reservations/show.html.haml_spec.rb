require 'rails_helper'

RSpec.describe "table_reservations/show", type: :view do
  before(:each) do
    @table_reservation = assign(:table_reservation, FactoryGirl.create(:table_reservation))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
  end
end
