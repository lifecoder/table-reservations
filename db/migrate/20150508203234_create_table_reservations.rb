class CreateTableReservations < ActiveRecord::Migration
  def change
    create_table :reservations do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.integer :table_number

      t.timestamps null: false
    end
  end
end
