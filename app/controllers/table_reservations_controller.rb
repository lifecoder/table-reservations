class TableReservationsController < ApplicationController
  before_action :set_table_reservation, only: [:show, :edit, :update, :destroy]

  # GET /table_reservations
  # GET /table_reservations.json
  def index
    @table_reservations = TableReservation.all
  end

  # GET /table_reservations/1
  # GET /table_reservations/1.json
  def show
  end

  # GET /table_reservations/new
  def new
    @table_reservation = TableReservation.new
  end

  # GET /table_reservations/1/edit
  def edit
  end

  # POST /table_reservations
  # POST /table_reservations.json
  def create
    @table_reservation = TableReservation.new(table_reservation_params)

    respond_to do |format|
      if @table_reservation.save
        format.html { redirect_to @table_reservation, notice: 'Table reservation was successfully created.' }
        format.json { render :show, status: :created, location: @table_reservation }
      else
        format.html { render :new }
        format.json { render json: @table_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /table_reservations/1
  # PATCH/PUT /table_reservations/1.json
  def update
    respond_to do |format|
      if @table_reservation.update(table_reservation_params)
        format.html { redirect_to @table_reservation, notice: 'Table reservation was successfully updated.' }
        format.json { render :show, status: :ok, location: @table_reservation }
      else
        format.html { render :edit }
        format.json { render json: @table_reservation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /table_reservations/1
  # DELETE /table_reservations/1.json
  def destroy
    @table_reservation.destroy
    respond_to do |format|
      format.html { redirect_to table_reservations_url, notice: 'Table reservation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_table_reservation
      @table_reservation = TableReservation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def table_reservation_params
      params.require(:table_reservation).permit(:start_time, :end_time, :table_number)
    end
end
