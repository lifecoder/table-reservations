json.array!(@table_reservations) do |table_reservation|
  json.extract! table_reservation, :id, :start_time, :end_time, :table_number
  json.url table_reservation_url(table_reservation, format: :json)
end
