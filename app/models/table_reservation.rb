class TableReservation < ActiveRecord::Base
  self.table_name = :reservations

  validates :start_time, :end_time, :table_number, presence: true
  validates :table_number, numericality: { only_integer: true, greater_than: 0 }
  validate :correct_datetimes_passed
  validate :timeframe_is_free

private

  def correct_datetimes_passed
    self.errors[:start_time] << "must be a valid date" unless DateTime.parse(self.start_time) rescue false
    self.errors[:end_time] << "must be a valid date" unless DateTime.parse(self.end_time) rescue false

    self.errors[:end_time] << "should be greater than a start date" unless (self.end_time > self.start_time) rescue false
  end

  def timeframe_is_free
    tr = TableReservation
        .where('table_number = ?', self.table_number)
        .where('(DATEDIFF(start_time, ?) * DATEDIFF(?, end_time)) >= 0', self.end_time, self.start_time)
        .where('NOT ((start_time > ?) OR (end_time < ?))', self.end_time, self.start_time)

    tr = tr.where('id != ?', self.id) if self.id

    puts tr.to_sql
    puts self.id.to_yaml
    puts tr.first.to_yaml
    is_free = tr.count == 0

    self.errors[:base] << 'This time may not be reserved due to the existing reservations' unless is_free
  end
end
